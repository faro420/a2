package a2;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Nico, G�tze> (<2125118>,<nico.goetze@haw-hamburg.de>),
 */
public class CelsiusToFahrenheit {
    public static void main(String args[]) {
        System.out.printf("Temperatur-Umrechnungstabelle\n");
        System.out.printf("=============================\n");
        System.out.printf("C    F\n");
        System.out.printf("----------\n");
        
        for (int celsius = 0; celsius <= 100; celsius += 5){
            System.out.printf("%d    %d\n",  celsius, (int) (celsius * 1.8 + 32 + 0.5));
        }
    }
}
